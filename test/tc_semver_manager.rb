# frozen_string_literal: true

require 'test/unit'
require 'assert2'
require 'semantic'
require_relative '../lib/semver_manager'

class String
  def prune_date
    gsub(/\d\d\d\d\-\d\d-\d\d/, '')
  end
end

# Test cllass for semver functionality
class TestSemverManager < Test::Unit::TestCase
  class << self
    def startup
      init_big
      init_small('/tmp/testrepo_clog')
      init_small('/tmp/testrepo_clog_v', 'ver')
    end

    def init_big
      # create simple test repo
      git = Git.init('/tmp/testrepo')
      Dir.glob('**/resources/**/*.txt').each { |f| FileUtils.copy(f, '/tmp/testrepo') }
      git.chdir do
        git.add('./file1.txt')
        git.add('file2.txt')
        git.commit('added first two files')
        git.add('file3.txt')
        git.add('file4.txt')
        git.commit("[added](added two more files)\n[changed](changed)")
        git.add_tag('version_latest', 'HEAD')
        git.add_tag('0.0.1', 'HEAD')
        git.add('file5.txt')
        git.commit('added yet another file')
      end
    end

    def init_small(path, version_prefix = '')
      # create a test-repo for testing the changelog functionality
      git = Git.init(path)
      Dir.glob('**/resources/**/*.txt').each { |f| FileUtils.copy(f, path) }
      git.chdir do
        git.add('file1.txt')
        git.commit('file 1')
        git.add_tag('version_latest', 'HEAD')
        git.add_tag("#{version_prefix}0.0.1", 'HEAD')
      end
    end

    def cleanup(path)
      FileUtils.remove_dir(path) if File.exist?(path)
    end

    def shutdown
      cleanup('/tmp/testrepo/')
      cleanup('/tmp/testrepo_clog/')
      cleanup('/tmp/testrepo_clog_v/')
    end
  end

  def extract_version
    begin
      options = {}
      options[:git_path] = '/tmp/testrepo'
      last = GitHandler.new(options).extract_last_version_from_tag
      ver = Semantic::Version.new last
    rescue Error
      assert { false }
    end
    ver
  end

  def bump(options)
    SemverManager.new(options).autobump(true)
  end

  def test_semver
    t = Semantic::Version.new '3.7.9'
    assert { t.increment!(:minor) == '3.8.0' }
  end

  # def test_autobump
  #   options = {}
  #   options[:git_path] = '/tmp/testrepo'
  #
  #   git = Git.open(options[:git_path])
  #
  #   bump(options)
  #
  #   ver = extract_version
  #   assert { ver.major.zero? }
  #   assert { ver.minor.zero? }
  #   assert { ver.patch == 2 }
  #
  #   FileUtils.copy('test/resources/file6.txt', options[:git_path])
  #   git.add('file6.txt')
  #   git.commit('added first two files [bump_major]')
  #
  #   bump(options)
  #
  #   ver = extract_version
  #   assert { ver.major == 1 }
  #   assert { ver.minor.zero? }
  #   assert { ver.patch.zero? }
  # end

  def test_changelog
    options = {}
    options[:git_path] = '/tmp/testrepo_clog'
    options[:clog_file] = 'CHANGELOG.md'
    options[:clog_full_path] = '/tmp/testrepo_clog/CHANGELOG.md'

    git = Git.open('/tmp/testrepo_clog')

    FileUtils.touch options[:clog_full_path]
    git.add('CHANGELOG.md')
    git.commit('[added](Introduction of new changelog file)')
    bump(options)

    git.add('file2.txt')
    git.commit('second file [skip ci]')
    bump(options)

    git.add('file3.txt')
    git.commit("third file [bump_major]\n[changed]{modified file}")
    bump(options)

    git.add('file4.txt')
    git.commit('fourth file [bump_minor]')
    bump(options)

    git.add('file5.txt')
    git.commit('third file [bump_major][fixed]{fixed some issue}')
    bump(options)

    git.add('file6.txt')

    logmsg = <<~LOGMSG
      sixth file [bump_major][fixed]{fixed some other issue}
      [added]{added awesome feature}\n[added]{added another feature}"
    LOGMSG

    git.commit(logmsg)
    bump(options)

    git.add('file7.txt')

    logmsg = <<~LOGMSG
      seventh file [bump_minor]
      [changed]{wohoo}
    LOGMSG

    git.commit(logmsg)
    bump(options)

    expect = <<~STR
      #### [3.1.0] 2020-03-25
      ##### Changed
      - wohoo
      #### [3.0.0] 2020-03-25
      ##### Added
      - added awesome feature
      - added another feature
      ##### Fixed
      - fixed some other issue
      #### [2.0.0] 2020-03-25
      ##### Fixed
      - fixed some issue
      #### [1.1.0] 2020-03-25
      #### [1.0.0] 2020-03-25
      ##### Changed
      - modified file
    STR

    assert { File.read(options[:clog_full_path]).prune_date == expect.prune_date }
  end

  def test_changelog_v
    options = {}

    options[:git_path] = '/tmp/testrepo_clog_v'
    options[:clog_file] = 'CHANGELOG.md'
    options[:clog_full_path] = '/tmp/testrepo_clog_v/CHANGELOG.md'
    options[:version_prefix] = 'ver'

    git = Git.open('/tmp/testrepo_clog_v')

    FileUtils.touch options[:clog_full_path]
    git.add('CHANGELOG.md')
    git.commit('[added](Introduction of new changelog file)')
    bump(options)

    git.add('file2.txt')
    git.commit('second file [skip ci]')
    bump(options)

    git.add('file3.txt')
    git.commit("third file [bump_major]\n[changed]{modified file}")
    bump(options)

    git.add('file4.txt')
    git.commit('fourth file [bump_minor]')
    bump(options)

    git.add('file5.txt')
    git.commit('third file [bump_major][fixed]{fixed some issue}')
    bump(options)

    git.add('file6.txt')

    logmsg = <<~LOGMSG
      sixth file [bump_major][fixed]{fixed some other issue}
      [added]{added awesome feature}\n[added]{added another feature}"
    LOGMSG

    git.commit(logmsg)
    bump(options)

    git.add('file7.txt')

    logmsg = <<~LOGMSG
      seventh file [bump_minor]
      [changed]{wohoo}
    LOGMSG

    git.commit(logmsg)
    bump(options)

    expect = <<~STR
      #### [ver3.1.0] 2020-03-25
      ##### Changed
      - wohoo
      #### [ver3.0.0] 2020-03-25
      ##### Added
      - added awesome feature
      - added another feature
      ##### Fixed
      - fixed some other issue
      #### [ver2.0.0] 2020-03-25
      ##### Fixed
      - fixed some issue
      #### [ver1.1.0] 2020-03-25
      #### [ver1.0.0] 2020-03-25
      ##### Changed
      - modified file
    STR

    assert { File.read(options[:clog_full_path]).prune_date == expect.prune_date }
  end
end
